'use strict';

// Declare app level module which depends on views, and components
var myApp = angular.module('myApp', [
  'ngRoute',
  'myApp.view1',
  'myApp.view2',
  'myApp.version'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.otherwise({redirectTo: '/view1'});
}]);


// testing




myApp.filter('reverse', function() {
 return function(input, uppercase) {
    input = input || '';
    var out = "";
    for (var i = 0; i < input.length; i++) {
      out = input.charAt(i) + out;
    }
    // conditional based on optional argument
    if (uppercase) {
      out = out.toUpperCase();
    }
    return out;
  };
});


myApp.filter('split', function () {
    
    return function (text) {
        return text.split('').join('_');
    };
    
});


var APP = angular.module('APP', []);


APP.ApplicationCtrl = function ($scope) {
    $scope.name = ''; 
    $scope.sayHelloWorld = function () {
        $scope.name = 'World :)';
    };
    $scope.sayHelloAngular = function () {
        $scope.name = 'Angular';
    };
    
    $scope.cities = [
        'Gdańsk',
        'Gdynia',
        'Sopot',
        'Szczecin',
        'Poznań',
        'Wrocław',
        'Kraków',
        'Łódź',
        'Bydgoszcz',
        'Toruń',
        'Opole',
        'Warszawa',
        'Białystok',
        'Olsztyn',
        'Lublin'
    ];
};

